FROM docker.io/existdb/existdb:6.2.0

# dependencies
ADD http://exist-db.org/exist/apps/public-repo/public/templating-1.1.0.xar /exist/autodeploy
# ediarum 4.2.0
# this file has to be built separatly. for a eXist 5.4.0+ compatible version see
# https://github.com/subugoe/ediarum.DB (or its PR to upstream)
ADD ediarum.db-4.2.0.xar /exist/autodeploy