🪿🪿🪿
🪿    Successfully installed {{ .Chart.Name }} into the {{ .Release.Namespace }} namespace.
🪿    We have set an admin password. You can retrieve with:
🪿    > kubectl -n {{ .Release.Namespace }} get configmaps exist-vault -o json | jq ".data[]"
🪿🪿🪿