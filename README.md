# Vier Wachen

A helm chart for „Vier Wachen“.

For Kubernetes.
Runs locally.

## Installation

```
NAME="vier-wachen"
# create a cluster exclusively for this project:
k3d cluster create ${NAME} -p "8000:80@loadbalancer" &&

# install the chart resources
helm install ${NAME} . --namespace ${NAME} --create-namespace &&

# set the contexts namespace for convenience
kubectl config set-context --current -- --namespace ${NAME}
```

Check the output of the `helm install` command.

## Development

Please use `helm-docs` for preparing the charts respective README.md files:
```
REPODIGEST="29fb3ec3ca32355d504e8655961b69acb2b1bf9e741d82b932f9468cf4cadd4e"
cd vier-wachen/ && docker run --rm --volume "$(pwd):/helm-docs" -u $(id -u) jnorwood/helm-docs@sha256:${REPODIGEST}
```
